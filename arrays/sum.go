package main

type Transaction struct {
	From string
	To string
	Sum float64
}

func BalanceFor(transactions []Transaction, name string) float64 {
	adjustBalance := func(currentBalance float64, t Transaction) float64 {
		if t.From == name {
			currentBalance -= t.Sum
		}
		if t.To == name {
			currentBalance += t.Sum
		}
		return currentBalance
	}

	return Reduce(transactions, adjustBalance, 0.0)
}

func Sum(numbers []int) (sum int) {
	return Reduce(numbers, func(acc, x int) int { return acc + x }, 0)
}

func SumAll(numbersToSum ...[]int) []int {
	return Reduce(numbersToSum, func(acc, x []int) []int {
		return append(acc, Sum(x))
	}, []int{})
}

func SumAllTails(numbersToSum ...[]int) []int {
	return Reduce(numbersToSum, func(acc, x []int) []int {
		if len(x) == 0 {
			acc = append(acc, 0)
		} else {
			tail := x[1:]
			acc = append(acc, Sum(tail))
		}
		return acc
	}, []int{})
}

func Reduce[A, B any](collection []A, accumulator func(B, A) B, initialValue B) B {
	var result = initialValue
	for _, x := range collection {
		result = accumulator(result, x)
	}

	return result
}

func Find[A any](collection []A, findFunc func(A) bool) (value A, isFound bool) {
	for _, x := range collection {
		if findFunc(x) {
			return x, true
		}
	}
	return
}
