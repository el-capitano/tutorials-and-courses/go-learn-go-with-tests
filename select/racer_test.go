package racer

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestRacer(t *testing.T) {
	t.Run("returns the url that resolves the fastest", func(t *testing.T) {
		slowServer := makeServer(20 * time.Millisecond)
		defer slowServer.Close()

		fastServer := makeServer(0 * time.Millisecond)
		defer fastServer.Close()

		slowURL := slowServer.URL
		fastURL := fastServer.URL

		want := fastURL
		got, err := Racer(slowURL, fastURL)

		if err != nil {
			t.Fatalf("did not expect an error but got one %v", err)
		}

		if got != want {
			t.Errorf("got %q, want %q", got, want)
		}
	})

	t.Run("Returns an error if a server doesn't respond within 10s", func(t *testing.T) {
		server := makeServer(2 * time.Microsecond)
		defer server.Close()

		_, err := ConfigurableRacer(server.URL, server.URL, 1 * time.Microsecond)

		if err == nil {
			t.Error("Expected an error after 10s but didn't get one")
		}

	})
}

func makeServer(delay time.Duration) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(delay)
		w.WriteHeader(http.StatusOK)
	}))
}
