package main_test

import (
	"fmt"
	"specifications/adapters"
	"specifications/adapters/grpcserver"
	"specifications/specifications"
	"testing"
)

func TestGreeterServer(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	var (
		port           = "50051"
		dockerFilePath = "./cmd/grpcserver/Dockerfile"

		driver = grpcserver.Driver{Addr: fmt.Sprintf("localhost:%s", port)}
	)

	adapters.StartDockerServer(t, port, dockerFilePath, "grpcserver")
	specifications.GreetSpecification(t, &driver)
	specifications.CurseSpecification(t, &driver)
}
