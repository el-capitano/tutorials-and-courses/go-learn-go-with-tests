package main

import (
	"log"
	"net/http"
	"specifications/adapters/httpserver"
)

func main() {
	if err := http.ListenAndServe(":8080", httpserver.NewHandler()); err != nil {
		log.Fatal(err)
	}
}
