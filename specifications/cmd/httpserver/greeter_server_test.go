package main_test

import (
	"fmt"
	"net/http"
	"specifications/adapters"
	"specifications/adapters/httpserver"
	"specifications/specifications"
	"testing"
	"time"
)

func TestGreeterServer(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	client := http.Client{
		Timeout: 1 * time.Second,
	}
	var (
		port = "8080"
		dockerFilePath = "./cmd/httpserver/Dockerfile"
		baseURL = fmt.Sprintf("http://localhost:%s", port)
		driver = httpserver.Driver{BaseURL: baseURL, Client: &client}
	)

	adapters.StartDockerServer(t, port, dockerFilePath, "httpserver")
	specifications.GreetSpecification(t, driver)
	specifications.CurseSpecification(t, &driver)
}
