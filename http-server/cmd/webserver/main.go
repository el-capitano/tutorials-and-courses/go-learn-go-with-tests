// cmd/webserver/main.go
package main

import (
	"log"
	"net/http"

	poker "example.com/hello/http-server"
)

const dbFileName = "game.db.json"

func main() {

	store, closeFunc, err := poker.NewFileSystemPlayerStoreFromFile(dbFileName)
	defer closeFunc()

	if err != nil {
		log.Fatal(err)
	}

	server := poker.NewPlayerServer(store)

	log.Fatal(http.ListenAndServe(":5000", server))
}
