package main

import (
	"fmt"
	"log"
	"os"

	poker "example.com/hello/http-server"
)

const dbFileName = "game.db.json"

func main() {
	fmt.Println("Let's play poker")
	fmt.Println("Type {Name} wins to record a win")

	store, closeFunc, err := poker.NewFileSystemPlayerStoreFromFile(dbFileName)
	defer closeFunc()

	if err != nil {
		log.Fatal(err)
	}

	game := poker.NewCLI(store, os.Stdin, poker.BlindAlerterFunc(poker.StdOutAlerter))

	game.PlayPoker()
}

