package poker

import (
	"bufio"
	"fmt"
	"io"
	"strings"
	"time"
)

type CLI struct {
	playerStore PlayerStore
	input       *bufio.Scanner
	output      io.Writer
	alerter     BlindAlerter
}

func NewCLI(store PlayerStore, input io.Reader, output io.Writer, alerter BlindAlerter) *CLI {
	return &CLI{
		playerStore: store,
		input:       bufio.NewScanner(input),
		output:      output,
		alerter:     alerter,
	}
}

func (cli *CLI) PlayPoker() {
	cli.scheduleBlindAlerts()

if _, err := fmt.Fprint(cli.output, "Please enter the number of players: "); err != nil {
	fmt.Errorf("error")
}

	userInput := cli.ReadLine()
	cli.playerStore.RecordWin(extractWinner(userInput))
}

func (cli *CLI) scheduleBlindAlerts() {
	blinds := []int{100, 200, 300, 400, 500, 600, 800, 1000, 2000, 4000, 8000}

	for i, blind := range blinds {
		blindTime := time.Duration(i*10) * time.Minute
		cli.alerter.ScheduleAlertAt(blindTime, blind)
	}
}

func (cli *CLI) ReadLine() string {
	cli.input.Scan()
	return cli.input.Text()
}

func extractWinner(userInput string) string {
	return strings.Replace(userInput, " wins", "", 1)
}
