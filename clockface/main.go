package main

import (
	"os"
	"time"

	clockface "example.com/hello/math"
)

func main() {
	t := time.Now()
	clockface.SVGWriter(os.Stdout, t)
}
