package main

import (
	"testing"
)

func TestWallet(t *testing.T) {
	t.Run("Deposit", func(t *testing.T) {
		wallet := Wallet{}

		wallet.Deposit(10)
		got := wallet.Balance()

		assert(t, got, 10)
	})

	t.Run("Withdraw with funds", func(t *testing.T) {
		wallet := Wallet{balance: 30}

		err := wallet.Withdraw(10)

		if err != nil {
			t.Fatal("Got an error but didn't want one")
		}
		got := wallet.Balance()

		assert(t, got, 20)
	})

	t.Run("Withdraw insufficient funds", func(t *testing.T) {
		wallet := Wallet{balance: 30}
		err := wallet.Withdraw(100)

		if err == nil {
			t.Error("wanted an error but didn't get one")
		}

		got := wallet.Balance()

		assert(t, got, 30)
	})
}

func assert(t testing.TB, got, want Bitcoin) {
	t.Helper()
	if got != want {
		t.Errorf("got %s want %s", got, want)
	}
}
