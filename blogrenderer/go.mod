module blogrenderer

go 1.21.6

require (
	github.com/approvals/go-approval-tests v1.1.0
	github.com/gomarkdown/markdown v0.0.0-20241205020045-f7e15b2f3e62
)
