package main

import (
	"reflect"
	"testing"
)

func TestPerimeter(t *testing.T) {
	got := Perimeter(Rectangle{10.0, 10.0})
	want := 40.0

	assert(t, got, want)

}

func TestArea(t *testing.T) {

	t.Run("Test Shapes", func(t *testing.T) {
		areaTests := []struct {
			shape Shape
			want float64
		} {
			{shape: Rectangle{12, 6}, want: 72.0},
			{shape: Circle{10}, want: 314.1592653589793},
			{shape: Triangle{12, 6}, want: 36.0},
		}

		for _, tt := range areaTests {
			shapeName := reflect.TypeOf(tt.shape).Name()
			t.Run(shapeName, func(t *testing.T) {
				got := tt.shape.Area()
				assert(t, got, tt.want)
			})
		}
	})
}

func assert(t testing.TB, got, want float64) {
	t.Helper()
	if got != want {
		t.Errorf("got %g want %g", got, want)
	}
}
